import './App.css';
import Calculate from './component/Calculate';

function App() {
  return (
    <div className="App">
      <Calculate />
    </div>
  );
}

export default App;
